import 'package:flutter/material.dart';
import 'package:training_logger/home_screen.dart';

void main() {
  runApp(TrainingLogger());
}

class TrainingLogger extends StatefulWidget {
  const TrainingLogger({super.key});

  @override
  State<TrainingLogger> createState() => _TrainingLoggerState();
}

class _TrainingLoggerState extends State<TrainingLogger> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: HomeScreen(),
      ),
    );
  }
}
